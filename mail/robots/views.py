import json
import requests

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse


# Create your views here.
@csrf_exempt
def query_book(request):
    if request.method == 'POST':
        res = dict()
        data = json.loads(request.body)

        query_word = data['text'].split(':')[1]
        payload = {'q': query_word, 'count': 1}
        response = requests.get('https://api.douban.com/v2/book/search', params=payload)

        res['text'] = response.json()['books']

        return HttpResponse(json.dumps(res))
