from django.conf.urls import url
from robots.views import query_book

urlpatterns = [
    url(r'^query/$', query_book, name='query_book'),
]
