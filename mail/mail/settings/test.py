# -*- coding: utf-8 -*-
from .base import *

DEBUG = True

TEMPLATE_DEBUG = True

# RQ_QUEUE settings
RQ_QUEUES = {
    'default': {
        'HOST': '192.168.100.30',
        'PORT': 6379,
        'DB': 2,
        'DEFAULT_TIMEOUT': 360,
    }
}

ALLOWED_HOSTS = []

STATIC_ROOT = '/data/web/resources/static/mail'
