# -*- coding: utf-8 -*-
from .base import *

DEBUG = True
INSTALLED_APPS += ('debug_toolbar',)

TEMPLATE_DEBUG = True

# RQ_QUEUE settings
RQ_QUEUES = {
    'default': {
        'HOST': 'localhost',
        'PORT': 6379,
        'DB': 0,
        'DEFAULT_TIMEOUT': 360,
    }
}

ALLOWED_HOSTS = []
