# -*- coding: utf-8 -*-
from django.db import models

from django.utils.translation import ugettext_lazy as _


from model_utils.fields import StatusField
from model_utils.models import TimeStampedModel
from model_utils import Choices


class EnvelopManager(models.Manager):

    def create_envelop(self, title, source_ip, content, receivers):
        envelop = self.model(title=title,
                             source_ip=source_ip,
                             content=content,
                             receivers=receivers)
        envelop.save(insert=True)
        return envelop


# Create your models here.
class Envelop(TimeStampedModel):

    """
    信件数据模型
    """
    STATUS = Choices('PENDING', 'SENT', 'FAILED')

    title = models.CharField(max_length=64, verbose_name=u'发送人')
    source_ip = models.GenericIPAddressField(verbose_name=u'来源IP地址')
    content = models.TextField(verbose_name=u'发送内容')
    receivers = models.CharField(max_length=256, verbose_name=u'收件人')

    status = StatusField(verbose_name=u'信件状态')

    objects = EnvelopManager()

    class Meta:
        verbose_name = _('envelop')
        verbose_name_plural = _('envelops')

    def save(self, **kwargs):
        if type(self.receivers) == list:
            self.receivers = ','.join(self.receivers)

        super(Envelop, self).save(kwargs)
