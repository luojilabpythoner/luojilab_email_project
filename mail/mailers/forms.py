# -*- coding:utf-8 -*-
from django import forms


class EnvelopForm(forms.Form):
    title = forms.CharField(required=True, label=u'邮件标题', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    content = forms.CharField(required=True, label=u'邮件内容', widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '5'}))
    receivers = forms.CharField(required=False, label=u'发送地址(逗号隔开)', widget=forms.TextInput(attrs={'class': 'form-control input-lg'}))
    use_admins = forms.BooleanField(required=False, label=u'是否使用管理员列表', widget=forms.TextInput(attrs={'type': 'checkbox', 'aria-label': ''}))

    def clean_receivers(self):
        receivers = self.cleaned_data.get('receivers')
        if receivers:
            if '@' not in receivers:
                raise forms.ValidationError("Invalid email provided")
            return receivers.split(',')

    def clean(self):
        super(EnvelopForm, self).clean()

        receivers = self.cleaned_data.get('receivers')
        use_admins = self.cleaned_data.get('use_admins')

        if not use_admins and not receivers:
            raise forms.ValidationError("No receivers specified")

        if use_admins and receivers:
            raise forms.ValidationError("Data conflicts")
