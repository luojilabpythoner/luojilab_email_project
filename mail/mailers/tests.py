
from django.conf import settings
from django.test import TestCase

from mailers.models import Envelop


# Create your tests here.
class EnvelopModelTestCase(TestCase):

    def test_can_create_envelop(self):
        sender = '1'
        receivers = settings.EMAIL_LIST
        source_ip = '192.168.100.20'
        content = {'name': 'fujian'}
        Envelop.objects.create_envelop(sender=sender,
                                       source_ip=source_ip,
                                       content=content,
                                       receivers=receivers)

        num_of_envelops = Envelop.objects.count()
        target_envelop = Envelop.objects.first()

        print target_envelop.receivers

        self.assertEqual(num_of_envelops, 1)
        self.assertEqual(target_envelop.receivers, ','.join(settings.EMAIL_LIST))
        self.assertEqual(target_envelop.content, content)
        print target_envelop.status
