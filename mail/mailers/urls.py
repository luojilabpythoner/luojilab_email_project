from django.conf.urls import url
from mailers.views import send_mail

urlpatterns = [
    url(r'^send/$', send_mail, name='send_mail'),
]
