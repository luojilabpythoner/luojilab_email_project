# -*- coding:utf-8 -*-
import json

from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from mailers.forms import EnvelopForm
from mailers.models import Envelop
from mailers.tasks import email_service

from llt.network import get_client_ip


def jsonResponse(code=0, message='success'):
    return json.dumps({'code': code,
                       'message': message})


@csrf_exempt
def send_mail(request):
    """/mailers/send/ 邮件发送接口

    调用方法:
        POST form-data

    参数:
        title (string): 邮件标题

        content (string): 邮件内容，json形式

        use_admins (bool): 是否使用默认管理员邮箱, 如果是True, receivers参数不用提交，使用已有管理员邮箱列表

        receivers(string, 可选): 发送地址，邮箱地址，并用逗号分开,如: fujian@luojilab.com, lining@luojilab.com

    返回（``json``）:
        code(int): 返回码，0为提交成功, 其他数字须查看详细信息

        message(str): 详细信息

    样例页面:
        http://mail.dev.didatrip.com/mailers/send/

    """
    if request.method == 'POST':
        envelop_form = EnvelopForm(request.POST)
        if envelop_form.is_valid():
            envelop = Envelop.objects.create_envelop(title=envelop_form.cleaned_data['title'],
                                                     source_ip=get_client_ip(request),
                                                     content=envelop_form.cleaned_data['content'],
                                                     receivers=settings.EMAIL_LIST if envelop_form.cleaned_data['use_admins'] else envelop_form.cleaned_data['receivers'])

            email_service.delay(envelop.pk)
            return HttpResponse(jsonResponse())
        else:
            print envelop_form.errors
            return render(request, 'home.html', {'form': envelop_form})
    else:
        return render(request, 'home.html', {'form': EnvelopForm()})
