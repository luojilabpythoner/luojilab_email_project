# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mailers', '0001_squashed_0002_auto_20151027_1843'),
    ]

    operations = [
        migrations.RenameField(
            model_name='envelop',
            old_name='sender',
            new_name='title',
        ),
    ]
