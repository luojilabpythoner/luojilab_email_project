# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import model_utils.fields
import django_extensions.db.fields.json


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Envelop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('sender', models.CharField(max_length=64, verbose_name='\u53d1\u9001\u4eba')),
                ('source_ip', models.GenericIPAddressField(verbose_name='\u6765\u6e90IP\u5730\u5740')),
                ('content', django_extensions.db.fields.json.JSONField(verbose_name='\u53d1\u9001\u5185\u5bb9')),
                ('receiver', models.CharField(max_length=256, verbose_name='\u6536\u4ef6\u4eba')),
                ('status', model_utils.fields.StatusField(default=b'PENDING', max_length=100, verbose_name='\u4fe1\u4ef6\u72b6\u6001', no_check_for_status=True, choices=[(b'PENDING', b'PENDING'), (b'SENT', b'SENT'), (b'FAILED', b'FAILED')])),
            ],
            options={
                'verbose_name': 'envelop',
                'verbose_name_plural': 'envelops',
            },
        ),
    ]
