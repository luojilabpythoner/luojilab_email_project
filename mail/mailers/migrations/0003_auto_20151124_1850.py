# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mailers', '0002_auto_20151028_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envelop',
            name='content',
            field=models.TextField(verbose_name='\u53d1\u9001\u5185\u5bb9'),
        ),
    ]
