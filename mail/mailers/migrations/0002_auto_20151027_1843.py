# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mailers', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='envelop',
            old_name='receiver',
            new_name='receivers',
        ),
    ]
