# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from django.conf import settings

from django_rq import job

from mailers.models import Envelop


@job
def email_service(pk):
    envelop = Envelop.objects.get(pk=pk)
    print type(envelop.content)
    result = send_mail(envelop.title,
                       envelop.content,
                       settings.EMAIL_HOST_USER,
                       envelop.receivers.split(','),
                       fail_silently=True,
                       html_message=envelop.content)
    if result == 1:
        envelop.status = Envelop.STATUS.SENT
    else:
        envelop.status = Envelop.STATUS.FAILED
    envelop.save()
    return envelop.status
