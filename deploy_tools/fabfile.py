# -*- coding: utf-8 -*-

from fabric.api import env, cd, run, prefix, settings
from fabric.contrib.files import exists
from contextlib import contextmanager


REPO_URL = "git@bitbucket.org:luojilabpythoner/luojilab_email_project.git"

env.name = 'mail'
env.guni_ip = '0.0.0.0'
env.guni_port = '8106'
env.guni_workers = 3

env.settings = {
    'test': {
        'hosts': ['python@192.168.100.30'],
        'path': '/home/python/projects/luojilab_email_project',
        'setting': 'test',
        'branch': 'master',
        'venv': '.env',
        'server_url': 'mail.dev.didatrip.com',
    }
}


def test():
    env.update(env.settings['test'])


def stop_app():
    with _virtualenv():
        with settings(warn_only=True):
            run('ps aux|grep %s.wsgi|grep -v \'grep\'|awk \'{ print $2 }\'|xargs kill -9' % env.name)


def start_app():
    with _virtualenv():
        run('gunicorn --env DJANGO_SETTINGS_MODULE=%s.settings.%s --daemon --chdir %s --workers %s --bind %s:%s %s.wsgi' % (env.name, env.setting, env.name, env.guni_workers, env.guni_ip, env.guni_port, env.name))


def start_worker():
    with _virtualenv():
        run('python %s/manage.py supervisor --daemonize --settings=%s.settings.%s' % (env.name, env.name, env.setting))


def stop_worker():
    with _virtualenv():
        run('python %s/manage.py supervisor shutdown --settings=%s.settings.%s' % (env.name, env.name, env.setting))


def restart_worker():
    stop_worker()
    start_worker()


def restart_app():
    stop_app()
    start_app()


def deploy():

    if not exists(env.path):
        _initialize_code()
    _create_directory_structure_if_necessary()
    with _virtualenv():
        _update_code()
        _install_package()
        _database_migration()
        _provide_initial_data()
        _collect_static_data()
    restart_worker()
    restart_app()


def _create_directory_structure_if_necessary():
    run('mkdir -p %s/%s' % (env.path, env.venv))


@contextmanager
def _virtualenv():

    with cd(env.path):
        if not exists('%s/bin/pip' % (env.venv,)):
            run('virtualenv %s --python=python2.7' % (env.venv,))
        with prefix('source %s/bin/activate' % (env.venv,)):
            yield


def _initialize_code():
    run('git clone %s %s' % (REPO_URL, env.path))
    with cd(env.path):
        run('git checkout %s' % env.branch)


def _update_code():
    run('git fetch')
    run('git pull')
    if run('git rev-parse --abbrev-ref HEAD') != env.branch:
        run('git checkout %s' % env.branch)


def _install_package():
    run('pip install --upgrade pip')
    run('pip install -r requirements/%s.txt' % env.setting)


def _database_migration():
    run('python %s/manage.py migrate --settings=%s.settings.%s --noinput' % (env.name, env.name, env.setting))


def _provide_initial_data():
    run('python %s/manage.py loaddata users.json --settings=%s.settings.%s' % (env.name, env.name, env.setting))


def _collect_static_data():
    run('python %s/manage.py collectstatic --settings=%s.settings.%s --noinput' % (env.name, env.name, env.setting))
