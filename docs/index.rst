.. luojilab_email_project documentation master file, created by
   sphinx-quickstart on Wed Oct 28 12:08:23 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to luojilab_email_project's documentation!
==================================================

This site covers email sending usage & API documentation.

API Address
-----------
For Testing Uses: http://mail.dev.didatrip.com


API List
--------
.. toctree::
   :maxdepth: 2

   modules/views
